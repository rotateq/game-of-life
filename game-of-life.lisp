(defpackage #:game-of-life
  (:use #:cl)
  ;; (:use #:clim-extensions #:clim #:clim-lisp)
  (:export #:main))

(in-package #:game-of-life)

(defparameter *cells-per-row* 80)
(defparameter *cells-per-column* 80)
(defparameter *cell-length* 5)
(defparameter *height* (+ 1 *cells-per-row* (* *cell-length* *cells-per-row*)))
(defparameter *width* (+ 1 *cells-per-column* (* *cell-length* *cells-per-column*)))

(defparameter *world* (make-array (list *cells-per-row* *cells-per-column*)
                                  :element-type 'bit
                                  :initial-element 0))

(defvar *Z* #2A((1 1 1 1 1)
                (0 0 0 1 0)
                (0 0 1 0 0)
                (0 1 0 0 0)
                (1 1 1 1 1)))

(defun fill-in-life-form (form)
  (setq *world* (funcall (april:april-c "{⍵ + 80 80 ↑ ⍺}") *world* form)))

(defun init-world ()
  (loop for i below *cells-per-row*
        do (loop for j below *cells-per-column*
                 do (setf (aref *world* i j) (random 2)))))

(defun update-world ()
  (setq *world* (funcall (april:april-c "{⊃ 1 ⍵ ∨.∧ 3 4 = +/ +⌿ 1 0 ¯1 ∘.⊖ 1 0 ¯1 ⌽¨ ⊂ ⍵}") *world*)))

(defun main ()
  (init-world)
  (sdl:with-init ()
    (flet ((draw-cell-square* (x y)
             "X and Y are the real coordinates in the pane."
             (sdl:draw-rectangle-* x y 5 5 :color sdl:*black*)))
      (sdl:window *height* *width* :title-caption "Game of Life")
      (setf (sdl:frame-rate) 10)
      (sleep 10)
      (sdl:with-events ()
        (:quit-event () t)
        ;; (:key-down-event () (fill-in-life-form *Z*))
        (:idle ()
               (sdl:clear-display sdl:*white*)
               (loop for i below *cells-per-row*
                     for row from 1 to (- *height* *cell-length*) by (1+ *cell-length*)
                     do (loop for j below *cells-per-column*
                              for col from 1 to (- *width* *cell-length*) by (1+ *cell-length*)
                              unless (zerop (aref *world* i j))
                                do (draw-cell-square* row col)))
               (sdl:update-display)
               ;; (sleep 0.1)
               (update-world))))))

#|
(define-application-frame world ()
  ()
  (:menu-bar nil)
  (:pane :application :display-function 'display :width *width* :height *height* :scroll-bars nil))

(defmethod display ((frame world) pane)
  (init-world)
  (flet ((draw-cell-square* (x y &key (ink +black+))
           "X and Y are the real coordinates in the pane."
           (draw-rectangle* pane x y (+ 5 x) (+ 5 y) :ink ink)))
    (tagbody
     iterate-world
       (loop for i below *cells-per-row*
             for row from 1 to (- *height* *cell-length*) by (1+ *cell-length*)
             do (loop for j below *cells-per-column*
                      for col from 1 to (- *width* *cell-length*) by (1+ *cell-length*)
                      if (= 1 (aref *world* i j))
                        do (draw-cell-square* row col)
                      else
                        do (draw-cell-square* row col :ink +white+)))
      (sleep 0.05)
      (update-world)
      (go iterate-world))))

(defun main ()
  (run-frame-top-level (make-application-frame 'world)))
|#
